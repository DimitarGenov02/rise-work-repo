﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Day4_Matrix_Algorithms;

namespace Day4_Testing
{
    [TestClass]
    public class SquaredThreeTests
    {
        [TestMethod]
        public void SquaredThreeTest1()
        {
            //Arrange
            int number = 1;
            //Act
            int base_of_cubed = SquaredThree.FindRoot3(number);
            //Assert
            Assert.AreEqual(1, base_of_cubed);
        }

        [TestMethod]
        public void SquaredThreeTest2()
        {
            //Arrange
            int number = 27;
            //Act
            int base_of_cubed = SquaredThree.FindRoot3(number);
            //Assert
            Assert.AreEqual(3, base_of_cubed);
        }

        [TestMethod]
        public void SquaredThreeTest3()
        {
            //Arrange
            int number = 2744;
            //Act
            int base_of_cubed = SquaredThree.FindRoot3(number);
            //Assert
            Assert.AreEqual(14, base_of_cubed);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "No whole number base of cube")]
        public void SquaredThreeTestNoBase()
        {
            int no_base = SquaredThree.FindRoot3(2);
        }
    }
}
