﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day4_Matrix_Algorithms
{
    public class SquaredThree
    {
        public static int FindRoot3(int num)
        {
            int i = 1;
            do
            {
                if (i*i*i == num)return i;
                i++;
                if (i > num) throw new Exception("No whole number base of cube"); // if(i*i*i > num) throw new ArgumentException(...);
            } while (true);
        }

    }
}
